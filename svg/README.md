The SVG icons in this directory were obtained from
[SVG Repo](https://www.svgrepo.com/). Except for adjusting the
size, they are unmodified.

The download links are:

- icon-close.svg:     https://www.svgrepo.com/svg/106704/close
- icon-minimize.svg:  https://www.svgrepo.com/svg/103811/full-screen
- icon-refresh.svg:   https://www.svgrepo.com/svg/85962/refresh

All three icons are distributed under a CC0 license.

The "maximize" icon was generated from the "minimize" icon by adding a vertical bar using Inkscape.
