;;;; Views for the CLOG inspector
;;
;;;; Copyright (c) 2024-2025 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package :clog-moldable-inspector)

;;
;; CLOG view class
;;

(defclass clog-view (hv:view)
  ((create-fn :reader create-fn :initarg :create-fn)))

(defmethod hv:view-references ((view clog-view))
  nil)

;;
;; Management of parametrized views and their variables
;;

(defclass view-state ()
  ((var-values :initform (fset:empty-map nil))
   (parametrized-subviews :initform (fset:empty-map (fset:empty-set)))))

(defun get-var-value (state place)
  (with-slots (var-values) state
    (if (consp place)
        (fset:@ (fset:@ var-values (car place)) (cdr place))
        (let ((value (fset:@ var-values place)))
          (if (typep value 'fset:seq)
              (fset:convert 'list value)
              value)))))

(defun set-var-value (state place value)
  (with-slots (var-values) state
    (if (consp place)
        (let* ((seq (or (fset:@ var-values (car place))
                        (fset:empty-seq nil))))
          (setf (fset:@ var-values (car place))
                (fset:with seq (cdr place) value)))
        (setf (fset:@ var-values place) value))))

(defun dependent-views (state place)
  (if (consp place)
      (fset:@ (slot-value state' parametrized-subviews) (car place))
      (fset:@ (slot-value state' parametrized-subviews) place)))

;;
;; Create a view element in the DOM
;;

(defgeneric create-view-element (pane parent-element view))

(defmethod create-view-element (pane parent-element (view clog-view))
  (funcall (create-fn view) pane parent-element))

(defmethod create-view-element (pane parent-element (view hv:html-view))
  (clog:js-execute parent-element
                   (format nil
                           "window.currentInspectorView = document.getElementById(\"~a\");"
                           (clog:html-id parent-element)))
  (dolist (asset (reverse (hv:view-assets view)))
    (case (car asset)
      (:path (clog-connection:add-plugin-path
              (str:concat "^" (cadr asset))
              (caddr asset)))
      (:js (clog:load-script
            (clog:html-document (clog:connection-body parent-element))
            (cdr asset)
            :load-only-once t))
      (:css (clog:load-css
             (clog:html-document (clog:connection-body parent-element))
             (cdr asset)
             :load-only-once t))))
  (setf (clog:inner-html parent-element) (hv:view-html view))
  ;; Run scripts from the asset list
  (dolist (asset (hv:view-assets view))
    (when (eq (car asset) :script)
      (blocking-js-execute parent-element (cdr asset))))
  ;; Set event handlers for object references etc.
  (set-event-handlers pane parent-element (hv:view-references view))
  ;; Process transclusions and interactive elements
  (let ((state (make-instance 'view-state)))
    ;; First pass: input elements
    (dolist (ref (hv:view-references view))
      (let* ((html-id (car ref))
             (html-id-parts (str:split "-" html-id))
             (ref-type (first html-id-parts))
             (target (cdr ref)))
        (when (and (string= ref-type "input")
                   target)
          ;; The target of an input reference is one of:
          ;;  - nil, meaning no variable is associated with the input element
          ;;  - a keyword naming a view variable
          ;;  - a cons pair of a keyword and an integer index, defining an item
          ;;    of an fset:seq that is the value of the variable named by the keyword 
          (set-var-value state target (funcall (get-input-fn parent-element) html-id))
          (clog:set-on-input
           (clog:attach-as-child parent-element html-id)
           #'(lambda (obj)
               (declare (ignore obj))
               (let ((hv:*get-input-fn* (get-input-fn parent-element))
                     (hv:*get-view-var-fn* #'(lambda (vv) (get-var-value state vv)))
                     (new-value (funcall (get-input-fn parent-element) html-id)))
                 (unless (equal new-value (get-var-value state target))
                   (set-var-value state target new-value)
                   (fset:do-set (pview (dependent-views state target))
                     (let* ((html-id (car pview))
                            (parent-div (clog:attach-as-child parent-element html-id)))
                       (create-view-element pane parent-div
                                            (-> pview cdr hv:view-thunk hv:eval-thunk)))))))))))
    ;; Second pass: transclusions and parametrized views. The latter
    ;; can only be processed once all view variables have been set
    ;; from the input elements, hence the need for a second pass.
    (dolist (ref (hv:view-references view))
      (let* ((html-id (car ref))
             (html-id-parts (str:split "-" html-id))
             (ref-type (first html-id-parts))
             (target (cdr ref)))
        (cond
          ;; The target of a transclusion is a view, which is inserted
          ;; as a child of the div referred to by the html-id.
          ((string= ref-type "transclusion")
           (let ((parent-div (clog:attach-as-child parent-element html-id)))
             (assert (typep target 'hv:view))
             (create-view-element pane parent-div target)))
          ;; The target of a psnippet reference is a
          ;; parametrized-subview object. The snippet is created by
          ;; calling the generating function, and inserted as a child
          ;; of the div referred to by the html-id.
          ((string= ref-type "pview")
           (let ((hv:*get-input-fn* (get-input-fn parent-element))
                 (hv:*get-view-var-fn* #'(lambda (vv) (get-var-value state vv)))
                 (parent-div (clog:attach-as-child parent-element html-id)))
             (assert (typep target 'hv:parametrized-subview))
             (create-view-element pane parent-div
                                  (-> target hv:view-thunk hv:eval-thunk))
             (with-slots (parametrized-subviews) state
               (dolist (var (hv:view-vars target))
                 (fset:includef (fset:@ parametrized-subviews var)
                                (cons html-id target)))))))))))

(defun blocking-js-execute (clog-obj js-code &key (wait-timeout 3))
  (let* ((sem (bordeaux-threads:make-semaphore))
         (document (clog:html-document (clog:connection-body clog-obj)))
         (token (symbol-name (gensym)))
         (code-with-trigger
           (format nil "~A;$(clog['document']).trigger('on-load-script','~A')"
                   js-code token)))
    (flet ((on-load (obj received-token)
             (declare (ignore obj))
             (when (equalp token received-token)
               (bordeaux-threads:signal-semaphore sem))))
      (clog:set-on-load-script document #'on-load :one-time t)
      (clog:js-execute clog-obj code-with-trigger)
      (clog:flush-connection-cache clog-obj)
      (bordeaux-threads:wait-on-semaphore sem :timeout wait-timeout)))
  ;; It's not as blocking as it should be. As a workaround, wait
  ;; for a second before continuing.
  (sleep 1))

;;
;; File content view for text files
;;

(defmethod text-view (pathname ace-mode)
  (make-instance 'clog-view
                 :title "Text"
                 :priority 1
                 :create-fn #'(lambda (pane parent-element)
                                (declare (ignore pane))
                                (ace-view parent-element pathname ace-mode))))

(defun ace-view (parent-element pathname ace-mode)
  (let ((ace-element (clog-ace:create-clog-ace-element parent-element)))
    (clog:set-geometry ace-element
                       :width (- (clog:client-width parent-element) 5)
                       :height (- (clog:client-height parent-element) 5))
    (clog-ace:resize ace-element)
    (setf (clog-ace:font-size ace-element) 14)
    (setf (clog-ace:text-value ace-element)
          (alexandria:read-file-into-string pathname))
    (setf (clog-ace:mode ace-element)
          (if ace-mode
              (str:concat "ace/mode/" ace-mode)
              (clog-ace:get-mode-from-extension ace-element pathname)))
    (setf (clog-ace:read-only-p ace-element) t)))

(defmethod hvs:file-content-view ((type (eql :lisp)) pathname)
  (text-view pathname "lisp"))

(defmethod hvs:file-content-view ((type (eql :asd)) pathname)
  (text-view pathname "lisp"))

(defmethod hvs:file-content-view ((type (eql :md)) pathname)
  (text-view pathname "markdown"))

(defmethod hvs:file-content-view ((type (eql :txt)) pathname)
  (text-view pathname "text"))

(defmethod hvs:file-content-view ((type (eql :html)) pathname)
  (text-view pathname "html"))

(defmethod hvs:file-content-view ((type (eql :js)) pathname)
  (text-view pathname "javascript"))

(defmethod hvs:file-content-view ((type (eql :css)) pathname)
  (text-view pathname "css"))

(defmethod hvs:file-content-view ((type (eql :txt)) pathname)
  (text-view pathname "text"))

(defmethod hvs:file-content-view ((type (eql :tex)) pathname)
  (text-view pathname "tex"))

(defmethod hvs:file-content-view ((type (eql :texi)) pathname)
  (text-view pathname "text"))

(defmethod hvs:file-content-view ((type (eql :scr)) pathname)
  (text-view pathname "text"))

(defmethod hvs:file-content-view ((type (eql :csv)) pathname)
  (text-view pathname "text"))

;;
;; Views on the clog-obj hierarchy
;;

(defmethod hv:text-representation ((element clog:clog-element))
  (format nil "~a id=~a"
          (-> element clog:html-tag str:downcase)
          (clog:html-id element)))

(hv:defview 👀tree (element clog:clog-element)
  (hv:html-view :title "Tree" :priority 1
    (hv:html
      (when-let (body (clog:connection-body element))
        (when-let (document (clog:html-document body))
          (cl-who:htm
           (:h4 "Document")
           (hv:object-ref document)))
        (cl-who:htm
         (:h4 "Body")
         (hv:object-ref body)))
      (when-let (parent (clog:parent element))
        (cl-who:htm
         (:h4 "Parent")
         (hv:object-ref parent)))
      (let ((first (clog:first-child element)))
        (unless (or (string= (clog:html-id first) "undefined")
                    (string= (clog:html-tag first) "undefined"))
          (cl-who:htm
           (:h4 "Children")
           (hv:html-table (child-elements element))))))))

(hv:defview 👀html (element clog:clog-element)
    (hv:html-code-view (hv:thunk (or (clog:outer-html element) ""))
                       :title "HTML" :priority 5))

;;
;; An attempt to see the JavaScript object "clog". Commented out
;; because it easily fills the heap on the Lisp side.
;;

;; (hv:defview 👀clog-objects (document clog:clog-document)
;;   (when-let (ids (-<> document
;;                    (clog:js-query "Object.getOwnPropertyNames(clog)")
;;                    (str:split "," <>)
;;                    (sort #'string<)))
;;     (hv:html-view :title "Clog objects" :priority 5
;;       (hv:html
;;         (:table :class "inspector-table"
;;                 (loop for id in ids do
;;                   (hv:html
;;                     (:tr (:td (hv:eval-button
;;                                id
;;                                (hv:thunk
;;                                 (clog::make-clog-element (clog::connection-id document) id))))))))))))
