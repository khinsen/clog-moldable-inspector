;;;; Playground view for the CLOG inspector
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package :clog-moldable-inspector)

;;
;; Eval a string in the context of an object
;;

(defun eval-in-context (string object)
  (let* ((*package* (ensure-playground-package object))
         (names (hvs:slot-names object))
         (local-names (mapcar #'(lambda (symbol)
                                  (list (-> symbol symbol-name intern) symbol))
                              names)))
    (eval `(let ((,(intern "*" *package*) ,object))
             (with-slots ,local-names ,object
               ,(with-input-from-string (input string)
                  (read input)))))))

(defun ensure-playground-package (object)
  (let* ((class-name (-> object class-of class-name))
         (base-package (-> class-name symbol-package))
         (base-package-name (-> base-package package-name))
         (playground-package-name (format nil "~a/~a-PLAYGROUND"
                                          base-package-name (symbol-name class-name))))
    (if-let (playground-package (find-package playground-package-name))
      playground-package
      (make-package playground-package-name
                    :use (list* base-package-name
                                "COMMON-LISP"
                                (package-use-list base-package))))))

;;
;; Playground class
;;

(defclass playground ()
  ((package :accessor playground-package :initarg :package)
   (cells :accessor playground-cells :initarg :cells)))

(defclass cell () ())

(defclass code-cell (cell)
  ((code :accessor cell-code :initarg :code)))

(defclass markdown-cell (cell)
  ((markdown :accessor cell-markdown :initform "")
   (html :accessor cell-html :initform "")))

(defun make-code-cell (code)
  (make-instance 'code-cell :code code))

(defun make-markdown-cell (markdown)
  (let ((cell (make-instance 'markdown-cell)))
    (replace-contents cell markdown)
    cell))

(defun replace-contents (markdown-cell markdown)
  (setf (cell-markdown markdown-cell) markdown)
  (setf (cell-html markdown-cell)
        (with-output-to-string (s)
          (-<> markdown
               3bmd-grammar:parse-doc
               (3bmd:print-doc-to-stream <> s :format :html)))))

(defun make-empty-playground (package)
  (make-instance 'playground
                 :package package
                 :cells (list
                         (make-code-cell ""))))

(defun insert-cell (playground index cell)
  (with-slots (cells) playground
    (assert (>= index 0))
    (assert (<= index (length cells)))
    (if (zerop index)
        (setf cells (cons cell cells))
        (push cell (cdr (nthcdr (- index 1) cells))))))

(defun delete-cell (playground index)
  (with-slots (cells) playground
    (assert (>= index 0))
    (assert (< index (length cells)))
    (if (zerop index)
        (setf cells (cdr cells))
        (let ((place (nthcdr (- index 1) cells)))
          (setf (cdr place) (cddr place))))))

;;
;; Read a markdown file into a playground
;;

(defun read-playground (package pathname)
  (let ((elements  (-> pathname
                       uiop:read-file-string
                       parse-markdown))
        cells)
    (labels ((code-cells (elements)
               (multiple-value-bind (code-cells tail)
                   (take-while #'code-block? elements)
                 (dolist (cell code-cells)
                   (let ((code-cell (make-code-cell (getf (cdr cell) :content))))
                     (push code-cell cells)))
                 (when tail
                   (non-code-cells tail))))
             (non-code-cells (elements)
               (multiple-value-bind (non-code-cells tail)
                   (take-while #'(lambda (el) (not (code-block? el))) elements)
                 (when non-code-cells
                   (dolist (cell (parse-cells non-code-cells))
                     (push (make-markdown-cell (unparse-markdown cell)) cells)))
                 (when tail
                   (code-cells tail)))))
      (non-code-cells elements))
    (make-instance 'playground
                   :package package
                   :cells (nreverse cells))))

(defun parse-markdown (string)
  (let ((3bmd-code-blocks:*code-blocks* t))
    (3bmd-grammar:parse-doc string)))

(defun unparse-markdown (elements)
  (str:trim-right
   (with-output-to-string (s)
     (3bmd:print-doc-to-stream elements s :format :markdown))))

(defun parse-cells (elements)
  (when elements
    (multiple-value-bind (cell tail)
        (take-while #'(lambda (el) (not (horizontal-rule? el))) elements)
      (cons cell (parse-cells (cdr tail))))))

(defun code-block? (element)
  (eq (car element) '3bmd-code-blocks::code-block))

(defun horizontal-rule? (element)
  (eq (car element) :horizontal-rule))

(defun take-while (predicate list)
  (let (acc)
    (loop :for sub :on list
          :do (unless (funcall predicate (car sub))
               (return (values (nreverse acc) sub)))
             (push (car sub) acc)
          :finally (return (values (nreverse acc) sub)))))

;;
;; Write a playground to a markdown file
;;

(defun save-playground (playground)
  (->> playground
       playground-package
       playground-cache-pathname
       (write-playground playground)))

(defun write-playground (playground pathname)
  (alexandria:with-output-to-file (stream pathname :if-exists :supersede)
    (loop :for cells :on (playground-cells playground)
          :do (progn
                (write-playground-cell (car cells) stream)
                (write-playground-cell-separator (car cells) (cadr cells) stream)))))

(defgeneric write-playground-cell (cell stream))

(defmethod write-playground-cell ((cell markdown-cell) stream)
  (write-string (cell-markdown cell) stream))

(defmethod write-playground-cell ((cell code-cell) stream)
  (let* ((code (cell-code cell))
         (code-to-write (if (str:empty? code) (format nil "~%") code)))
    (format stream "```lisp~%~a~&```~%" code-to-write)))

(defgeneric write-playground-cell-separator (cell1 cell2 stream))

(defmethod write-playground-cell-separator ((cell1 t) (cell2 t) stream)
  (declare (ignore cell1 cell2))
  (format stream "~&~%"))

(defmethod write-playground-cell-separator ((cell1 markdown-cell)
                                            (cell2 markdown-cell)
                                            stream)
  (declare (ignore cell1 cell2))
  (format stream "~&~%---~%~%"))

;;
;; Persistent storage of playground contents
;;

(defun playground-cache-directory ()
  (-> (merge-pathnames #P".cache/clog-malleable-inspector/"
                       (user-homedir-pathname))
      ensure-directories-exist))

(defun playground-cache-pathname (package)
  (-<> package
    package-name
    hv:encode-base32
    (str:concat <> ".md")
    (merge-pathnames (playground-cache-directory))))

(defun make-playground (package)
  (let ((pathname (playground-cache-pathname package)))
    (if (probe-file pathname)
        (read-playground package pathname)
        (make-empty-playground package))))

;;
;; Playground view
;;

(hv:defview 👀playground (object t)
  (make-instance 'clog-view
                 :title "Playground"
                 :priority 100
                 :create-fn #'(lambda (pane parent-element)
                                (create-playground-view pane parent-element))))

;; Suppress the view for GUI classes
(hv:defview 👀playground (object hv:gui-class)
  nil)

(defun create-playground-view (pane parent-element)
  (with-slots (object) pane
    (let* ((pg-package (-> object ensure-playground-package))
           (pg (-> pg-package make-playground))
           (view-element (clog:create-div parent-element
                                          :class "inspector-playground"))
           (sections '()))
      (pg-package-reference pane view-element pg-package)
      (clog:create-hr view-element)
      (dolist (cell (playground-cells pg))
        (let ((section-element (create-playground-view-section
                                pg cell view-element pane
                                #'(lambda (el) (clog:place-inside-bottom-of 
                                                view-element el)))))
          (push section-element sections)))
      (setf sections (nreverse sections))
      (when-let (first-coder (find-if #'coder? sections))
        (clog:focus first-coder)))))

(defun coder? (element)
  (-<> element
       clog:css-class-name
       (str:split " " <>)
       (member "inspector-code-cell" <> :test #'string=)))

(defun pg-package-reference (pane parent-element package)
  (with-slots (object inspector clog-obj) pane
    (let* ((div (clog:create-div parent-element))
           (_ (clog:create-span div :content "<i>Package:</i> "))
           (button (clog:create-span div
                                     :class "inspector-inspect"
                                     :content (-> package package-name str:downcase))))
      (declare (ignore _))
      (setf (clog:attribute div "align") "center")
      (clog:set-on-mouse-click
       button
       #'(lambda (obj event)
           (declare (ignore obj))
           (unless (getf event :shift-key)
             (close-panes-after inspector pane))
           (create-pane inspector package))))))

(defgeneric create-playground-view-section (playground
                                            cell
                                            parent-element
                                            pane
                                            place-fn))

(defmethod create-playground-view-section (playground
                                           (cell markdown-cell)
                                           parent-element
                                           pane
                                           place-fn)
  (let ((div (clog:create-div parent-element
                              :class "inspector-markdown-cell"
                              :auto-place nil)))
    (funcall place-fn div)
    (setf (clog:attribute div "tabindex") "0")
    (let* ((rendered (clog:create-div div :content (cell-html cell)))
           (ace (clog-ace:create-clog-ace-element div)))
      (setf (clog:hiddenp ace) t)
      ;; Configure rendered view
      (clog:set-on-key-press
       div ;; It's the div that has tabindex=0,
           ;; which allows it to receive keypress events.
       #'(lambda (obj event)
           (declare (ignore obj))
           ;; Process key events only if the editor does not accept keyboard
           ;; input.
           (unless (string= (clog:js-query div "document.activeElement.className")
                            "ace_text-input")
             (alexandria:switch ((getf event :key) :test #'string=)
               ("Enter" (unless (clog:hiddenp rendered)
                          (setf (clog:hiddenp rendered) t)
                          (setf (clog:hiddenp ace) nil)
                          (clog:focus ace)))
               ("a" (playground-insert-above playground cell
                                             div parent-element
                                             pane :markdown))
               ("b" (playground-insert-below playground cell
                                             div parent-element
                                             pane :markdown))
               ("x" (playground-delete playground cell div))))))
      (clog:set-on-mouse-double-click
       rendered
       #'(lambda (obj event)
           (declare (ignore obj event))
           (setf (clog:hiddenp rendered) t)
           (setf (clog:hiddenp ace) nil)))
      ;; Configure editor view
      (setf (clog-ace:mode ace) "ace/mode/markdown")
      (setf (clog-ace:font-size ace) 16)
      (clog-ace:set-option ace "minLines" "1")
      (clog-ace:set-option ace "maxLines" "Infinity")
      (clog-ace:set-option ace "enableKeyboardAccessibility" "true")
      (clog:js-execute ace
                       (format nil "~A.renderer.setShowGutter(false)"
                               (clog-ace::js-ace ace)))
      (clog:js-execute parent-element
                       (format nil
                               "window.currentAceEditor = ~A;"
                               (clog-ace::js-ace ace)))
      (clog:js-execute ace
                       (format nil
                               "~A.commands.addCommand(
                                   {name: \"hideYourself\",
                                    bindKey: \"Esc\",
                                    exec: function(editor) {
                                       clog['~A'].hidden = true;
                                       clog['~A'].hidden = false;}});"
                               (clog-ace::js-ace ace)
                               (clog:html-id ace)
                               (clog:html-id rendered)))
      (clog:set-geometry ace :width (- (clog:client-width parent-element) 5))
      (setf (clog-ace:text-value ace) (cell-markdown cell))
      (clog-ace:resize ace)
      (clog:set-on-change
       ace
       #'(lambda (obj)
           (declare (ignore obj))
           (replace-contents cell (clog-ace:text-value ace))
           (setf (clog:inner-html rendered) (cell-html cell))
           (save-playground playground))))
    div))

(defmethod create-playground-view-section (playground
                                           (cell code-cell)
                                           parent-element
                                           pane
                                           place-fn)
  (let ((div (clog:create-div parent-element
                              :class "inspector-code-cell"
                              :auto-place nil)))
    (funcall place-fn div)
    (let* ((ace (clog-ace:create-clog-ace-element div))
           (eval-button (clog:create-button div :content "Eval")))
      (setf (clog-ace:mode ace) "ace/mode/lisp")
      (setf (clog-ace:font-size ace) 16)
      (clog-ace:set-option ace "minLines" "1")
      (clog-ace:set-option ace "maxLines" "Infinity")
      (clog-ace:set-option ace "enableKeyboardAccessibility" "true")
      (clog:js-execute ace
                       (format nil "~A.renderer.setShowGutter(false)"
                               (clog-ace::js-ace ace)))
      (clog:set-geometry ace :width (- (clog:client-width parent-element) 5))
      (setf (clog-ace:text-value ace) (cell-code cell))
      (clog-ace:resize ace)
      (clog:set-on-mouse-click
       eval-button
       #'(lambda (obj event)
           (declare (ignore obj))
           (let* ((selection (clog-ace:selected-text ace))
                  (text (if (zerop (length selection))
                            (clog-ace:text-value ace)
                            selection)))
             (with-slots (inspector object) pane
               (unless (getf event :shift-key)
                 (close-panes-after inspector pane))
               (create-pane inspector (-> text
                                          (eval-in-context object)))))))
      (clog:set-on-change
       ace
       #'(lambda (obj)
           (declare (ignore obj))
           (setf (cell-code cell) (clog-ace:text-value ace))
           (save-playground playground)))
      (clog:js-execute ace
                       (format nil
                               "~A.commands.addCommand(
                                   {name: \"evalLisp\",
                                    bindKey: \"Shift-Enter\",
                                    exec: function(editor) {
                                       clog['~A'].click();}});"
                               (clog-ace::js-ace ace)
                               (clog:html-id eval-button)))
      (clog:set-on-key-press
       ace
       #'(lambda (obj event)
           (declare (ignore obj))
           ;; Process key events only if the editor does not accept keyboard
           ;; input.
           (unless (string= (clog:js-query div "document.activeElement.className")
                            "ace_text-input")
             (alexandria:switch ((getf event :key) :test #'string=)
               ("a" (playground-insert-above playground cell
                                             div parent-element
                                             pane :lisp))
               ("b" (playground-insert-below playground cell
                                             div parent-element
                                             pane :lisp))
               ("x" (playground-delete playground cell div))))))
      (clog:set-on-focus
       div
       #'(lambda (obj)
           (declare (ignore obj))
           (clog:focus ace)))
      div)))

;;
;; Insert and delete cells
;;

(defun playground-insert-above (playground
                                cell
                                current-element
                                parent-element
                                pane
                                cell-type)
  (let* ((current-index (search (list cell)
                                 (playground-cells playground)
                                 :test #'eq))
         (new-cell (new-empty-cell cell-type)))
    (insert-cell playground current-index new-cell)
    (with-slots (object) pane
      (clog:focus
       (create-playground-view-section
        playground new-cell parent-element pane
        #'(lambda (el) (clog:place-before current-element el))))))
  (save-playground playground))

(defun playground-insert-below (playground
                                cell
                                current-element
                                parent-element
                                pane
                                cell-type)
  (let* ((current-index (search (list cell)
                                 (playground-cells playground)
                                 :test #'eq))
         (new-cell (new-empty-cell cell-type)))
    (insert-cell playground (+ 1 current-index) new-cell)
    (with-slots (object) pane
      (clog:focus
       (create-playground-view-section
        playground new-cell parent-element pane
        #'(lambda (el) (clog:place-after current-element el))))))
  (save-playground playground))

(defun new-empty-cell (cell-type)
  (case cell-type
    (:lisp (make-code-cell ""))
    (:markdown (make-markdown-cell ""))))

(defun playground-delete (playground cell current-element)
  (let* ((current-index (search (list cell)
                                 (playground-cells playground)
                                 :test #'eq)))
    (delete-cell playground current-index)
    (clog:destroy current-element))
  (save-playground playground))

;;
;; Playground contents and playground object view (for debugging)
;;

;; (hv:defview 👀playground-markdown (object t)
;;   (hv:rename (hvs:👀content (-> object
;;                                ensure-playground-package
;;                                playground-cache-pathname))
;;              :title "PG Markdown" :priority 101))

;; (hv:defview 👀playground-parsed (object t)
;;   (let ((file (-> object ensure-playground-package playground-cache-pathname)))
;;     (when (probe-file file)
;;       (hv:rename (hv:👀items (-> file
;;                                 uiop:read-file-string
;;                                 parse-markdown))
;;                  :title "PG Parse tree" :priority 102))))

;; (hv:defview 👀playground-cell (object t)
;;   (hv:rename (hv:👀items (-> object
;;                             ensure-playground-package
;;                             make-playground
;;                             playground-cells))
;;              :title "PG cells" :priority 103))
