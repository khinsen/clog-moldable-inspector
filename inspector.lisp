;;;; Moldable inspector based on CLOG
;;
;;;; Copyright (c) 2024-2025 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package :clog-moldable-inspector)

;;
;; Inspector class
;;

(defclass inspector ()
  ((panes :accessor inspector-panes :initform (fset:empty-seq))
   (active-pane :accessor inspector-active-pane :initform nil)
   (pane-width :accessor inspector-pane-width :initarg :pane-width)
   (clog-obj :accessor clog-obj  :initarg :clog-obj)))

(defun create-inspector (parent-obj &key (pane-width "600px"))
  (make-instance 'inspector
                 :clog-obj (clog:create-div parent-obj
                                            :class "inspector")
                 :pane-width pane-width))

(defgeneric add-pane (inspector object))
(defgeneric close-pane (inspector pane))
(defgeneric close-panes-after (inspector pane))
(defgeneric minimize-pane (inspector pane))
(defgeneric maximize-pane (inspector pane))

(defmethod hv:text-representation ((inspector inspector))
  (format nil "Inspector with ~d panes" (fset:size (inspector-panes inspector))))

(hv:defview 👀panes (inspector inspector)
  (-<> inspector
       inspector-panes
       (fset:convert 'list <>)
       (hv:list-view :title "Panes" :priority 1)))

;;
;; Pane class
;;

(defclass pane ()
  ((inspector :reader inspector :initarg :inspector)
   (object :accessor pane-object :initarg :object)
   (title-bar :accessor pane-title-bar)
   (action-buttons :accessor pane-action-buttons)
   (views :accessor pane-views :initform (fset:empty-seq))
   (active-view :accessor pane-active-view :initform 0)
   (maximized? :accessor pane-maximized? :initform nil)
   (clog-obj :accessor clog-obj)
   (tab-ids :accessor pane-tab-ids)
   (view-ids :accessor pane-view-ids)))

(defgeneric minimize (pane))
(defgeneric maximize (pane))
(defgeneric refresh (pane))
(defgeneric select-view (pane view-index-or-title))

(defmethod hv:text-representation ((pane pane))
  (format nil "Pane: ~a" (hv:text-representation (pane-object pane))))

(hv:defview 👀title-bar (pane pane)
  (with-slots (title-bar action-buttons) pane
    (hv:html-view :title "Title bar" :priority 1
      (hv:html
        (:h4 "Title")
        (:div :id (hv:inspect-id title-bar)
              (:span :class "inspector-inspect"
                     (cl-who:str (hv:view-html title-bar))))
        (let ((ab-html (hv:view-html action-buttons)))
          (when (> (length ab-html) 0)
            (hv:html
             (:h4 "Action buttons")
             (:div :id (hv:inspect-id action-buttons)
                   (:span :class "inspector-inspect"
                      (cl-who:str ab-html))))))))))

(hv:defview 👀views (pane pane)
  (let ((views (-> pane pane-views))
        (view-elements (-> pane clog-obj child-elements third child-elements)))
    (hv:html-view :title "Views" :priority 2
      (hv:html
        (:table :class "inspector-table"
                (:tr (:th "View") (:th "DOM element"))
                (loop for v in views
                      for ve in view-elements
                      do (hv:html
                           (:tr (:td (hv:object-ref v))
                                (:td (hv:object-ref ve))))))))))

;;
;; Methods for inspector operations
;;

(defmethod add-pane ((inspector inspector) pane)
  (with-slots (panes active-pane) inspector
    (callf panes #'fset:with-last pane)
    (setf active-pane pane)))

(defmethod close-pane ((inspector inspector) (pane pane))
  (with-slots (panes active-pane) inspector
    (let ((index (fset:position pane panes :test #'eq)))
      (some-> (fset:@ panes index)
              clog-obj
              clog:destroy)
      (callf panes #'fset:less index)
      (when (eq pane active-pane)
        ;; make the pane to the left active, or else the leftmost pane
        (setf active-pane 
              (if (= 0 (fset:size panes))
                  nil
                  (fset:@ panes (max 0 (- index 1)))))
        (when active-pane
          (clog:focus (clog-obj active-pane)))))))

(defmethod close-panes-after ((inspector inspector) (pane pane))
  (with-slots (panes active-pane) inspector
    (let ((index (fset:position pane panes :test #'eq)))
      (when index
        (loop for i from (- (fset:size panes) 1) above index
              do (-> (fset:@ panes i)
                     clog-obj
                     clog:destroy))
        (setf panes (fset:subseq panes 0 (+ 1 index)))))
    (setf active-pane pane)
    (clog:focus (clog-obj active-pane))))

(defmethod minimize-pane ((inspector inspector) (pane pane))
  (with-slots (panes pane-width) inspector
    (fset:do-seq (some-pane panes)
      (setf (clog:hiddenp (clog-obj some-pane)) nil))
    (clog:remove-class (clog-obj pane) "inspector-pane-maximized")
    (clog:add-class (clog-obj pane) "inspector-pane")
    (setf (clog:attribute (clog-obj pane) "style")
          (format nil "flex-basis: ~a; min-width: min(~a, 95%);"
                  pane-width pane-width))))

(defmethod maximize-pane ((inspector inspector) (pane pane))
  (with-slots (panes pane-width) inspector
    (fset:do-seq (some-pane panes)
      (setf (clog:hiddenp (clog-obj some-pane))
            (not (eq some-pane pane))))
    (clog:remove-class (clog-obj pane) "inspector-pane")
    (clog:add-class (clog-obj pane) "inspector-pane-maximized")
    (setf (clog:attribute (clog-obj pane) "style")
          (format nil "flex-basis: 100%; min-width: min(~a, 95%);" pane-width))))

;;
;; Methods for pane operations
;;

(defun create-pane (inspector object &key (select 0))
  (with-slots (panes) inspector
    (fset:do-seq (some-pane panes)
      (minimize some-pane)))
  (let ((pane (make-instance 'pane
                             :inspector inspector
                             :object object))
        (style-attr (format nil "flex-basis: ~a; min-width: min(~a, 95%);"
                            (inspector-pane-width inspector)
                            (inspector-pane-width inspector))))
    (setf (clog-obj pane) (clog:create-div (clog-obj inspector)
                                           :class "inspector-pane"
                                           :style style-attr))
    (setf (clog:attribute (clog-obj pane) "tabindex") "0")
    (clog:focus (clog-obj pane))
    (load-views pane)
    (create-dom pane)
    (add-pane inspector pane)
    (select-view pane select)
    pane))

(defun load-views (pane)
  (with-slots (object title-bar action-buttons views clog-obj tab-ids view-ids) pane
    (setf title-bar (hv:title-bar object))
    (setf action-buttons (hv:title-bar-action-buttons object))
    (setf views (hv:all-views object))
    (setf tab-ids (mapcar #'(lambda (x)
                              (declare (ignore x))
                              (gensym "tab"))
                          views))
    (setf view-ids (mapcar #'(lambda (x)
                               (declare (ignore x))
                               (gensym "view"))
                           views))))

(defmethod minimize ((pane pane))
  (when (pane-maximized? pane)
    (minimize-pane (inspector pane) pane)
    (setf (pane-maximized? pane) nil)))

(defmethod maximize ((pane pane))
  (unless (pane-maximized? pane)
    (maximize-pane (inspector pane) pane)
    (setf (pane-maximized? pane) t)))

(defmethod refresh ((pane pane))
  (dolist (element (child-elements (clog-obj pane)))
    (clog:destroy element))
  (load-views pane)
  (create-dom pane)
  (select-view pane (pane-active-view pane)))

(defun child-elements (element)
  (loop for child = (clog:first-child element) then (clog:next-sibling child)
        until (string= (clog:html-id child) "undefined")
        collect child))

(defmethod select-view ((pane pane) view-index-or-title)
  (with-slots (clog-obj views tab-ids view-ids active-view) pane
    (let ((view-index 0))
      (if (numberp view-index-or-title)
          (setf view-index view-index-or-title)
          (loop for view in views
                for index from 0
                do (when (string= (hv:view-title view) view-index-or-title)
                     (setf view-index index))))
      (loop for view-id in view-ids
            for tab-id in tab-ids
            for index from 0
            do (let ((active? (equal index view-index))
                     (tab (clog:attach-as-child clog-obj tab-id)))
                 (if active?
                     (clog:add-class tab "active")
                     (clog:remove-class tab "active"))
                 (setf (clog:hiddenp (clog:attach-as-child clog-obj view-id))
                       (not active?))))
      (let ((view-element (clog:attach-as-child clog-obj (nth view-index view-ids))))
        (when (string= (-> view-element clog:first-child clog:html-id) "undefined")
          (create-view-element pane view-element (nth view-index views))))
      (setf active-view view-index))))

;;
;; Create the DOM representation of a pane
;;

(defun create-dom (pane)
  (with-slots (clog-obj object views view-ids) pane
    (let* ((parent (clog:parent-element clog-obj))
           (title-bar (create-title-bar pane))
           (tab-bar (create-tabs pane))
           (body (clog:create-div clog-obj :class "inspector-body")))
      (declare (ignore title-bar tab-bar))
      (dolist (id view-ids)
        (clog:create-div body :html-id id :class "inspector-view"))
      (setf (clog:scroll-left parent)
            (- (clog:scroll-width parent) (clog:client-width parent))))))

(defun create-title-bar (pane)
  (with-slots (clog-obj object inspector title-bar action-buttons) pane
    (let ((bar (clog:create-div clog-obj :class "inspector-title-bar")))
      (let ((title (clog:create-span bar :class "inspector-title")))
        (setf (clog:inner-html title) (hv:view-html title-bar))
        (set-event-handlers pane title (hv:view-references title-bar)))
      (clog:create-span bar :class "inspector-title-bar-separator")
      (let ((action (clog:create-span bar
                                      :class "inspector-title-bar-action-buttons")))
        (setf (clog:inner-html action) (hv:view-html action-buttons))
        (set-event-handlers pane action (hv:view-references action-buttons)))
      (create-title-bar-button bar *icon-refresh* (hv:thunk (refresh pane)))
      (create-title-bar-button bar *icon-maximize* (hv:thunk (maximize pane))
                               :classes "inspector-button inspector-maximize-button")
      (create-title-bar-button bar *icon-minimize* (hv:thunk (minimize pane))
                               :classes "inspector-button inspector-minimize-button")
      (create-title-bar-button bar *icon-close* (hv:thunk (close-pane inspector pane)))
      (clog:set-on-mouse-click bar
                               #'(lambda (obj event)
                                   (declare (ignore obj))
                                   (when (getf event :alt-key)
                                     (unless (getf event :shift-key)
                                       (close-panes-after inspector pane))
                                     (create-pane inspector pane))))
      bar)))

(defun create-title-bar-button (bar content thunk &key (classes "inspector-button"))
  (let ((button (clog:create-button bar :class classes
                                        :content content)))
    (clog:set-on-click button #'(lambda (obj)
                                  (declare (ignore obj))
                                  (hv:eval-thunk thunk)))))

(defun create-tabs (pane)
  (with-slots (clog-obj inspector views tab-ids) pane
    (let ((view-titles (mapcar #'hv:view-title views))
          (tabs (clog:create-div clog-obj :class "inspector-tabs")))
      (loop for tab-text in view-titles
            for tab-id in tab-ids
            for view in views
            for index from 0
            do (let* ((tab (clog:create-button tabs
                                               :content tab-text
                                               :html-id tab-id))
                      (view* view)
                      (index* index))
                 (clog:set-on-mouse-click
                  tab
                  #'(lambda (obj event)
                      (declare (ignore obj))
                      (if (getf event :alt-key)
                          (progn
                            (unless (getf event :shift-key)
                              (close-panes-after inspector pane))
                            (create-pane inspector view* :select "Source code"))
                          (progn
                            (select-view pane index*)))))))
      tabs)))

;;
;; Define the mouse click actions for the object references
;; embedded in a DOM element.
;;

(defun set-event-handlers (pane element references)
  (with-slots (object inspector clog-obj) pane
    (dolist (ref references)
      (let* ((target (cdr ref))
             (html-id (car ref))
             (html-id-parts (str:split "-" html-id))
             (ref-type (first html-id-parts))
             (ref-element (clog:attach-as-child element html-id)))
        (cond
          ;; Inspect: the target is the object for which a new inspector
          ;; pane is opened. If the id has a third part (i.e. contains two dashes),
          ;; the third part is taken to be the base32-encoded name of the view
          ;; to be selected in the new pane.
          ((string= ref-type "inspect")
           (let ((view-ref nil))
             (when (eql (length html-id-parts) 3)
               (setf view-ref (hv:decode-base32 (third html-id-parts))))
             (clog:set-on-mouse-click
              ref-element
              #'(lambda (obj event)
                  (declare (ignore obj))
                  (when (getf event :alt-key)
                    (clog:jquery-trigger (clog:parent element) "click"))
                  (unless (getf event :alt-key)
                    (unless (getf event :shift-key)
                      (close-panes-after inspector pane))
                    (create-pane inspector target :select view-ref)))
              :cancel-event t)))
          ;; Action: the target is a thunk that is evaluated. If its
          ;; return value is true-ish, the object's pane is refreshed.
          ((string= ref-type "action")
           (clog:set-on-mouse-click
            ref-element
            #'(lambda (obj event)
                (if (getf event :alt-key)
                    (progn
                      (unless (getf event :shift-key)
                        (close-panes-after inspector pane))
                      (create-pane inspector target))
                    (when (eval-thunk-with-active-button clog-obj obj target)
                      (refresh pane))))
            :cancel-event t))
          ;; Eval: the target is a thunk that is evaluated. A new inspector
          ;; pane is opened for the return value.
          ((string= ref-type "eval")
           (clog:set-on-mouse-click
            ref-element
            #'(lambda (obj event)
                (unless (getf event :shift-key)
                  (close-panes-after inspector pane))
                (if (getf event :alt-key)
                    (create-pane inspector target)
                    (create-pane inspector
                                 (eval-thunk-with-active-button clog-obj obj target))))
            :cancel-event t))
          ;; Transclusion: the target is a view that is transcluded
          ;; (see create-view-element). On alt-click, a new inspector
          ;; pane is opened for the object underlying this view.
          ((string= ref-type "transclusion")
           (clog:set-on-mouse-click
            ref-element
            #'(lambda (obj event)
                (declare (ignore obj))
                (when (getf event :alt-key)
                  (unless (getf event :shift-key)
                    (close-panes-after inspector pane))
                  (create-pane inspector (hv:view-object target))))
            :cancel-event t)))))))

(defun eval-thunk-with-active-button (pane-clog-obj obj target)
  (clog:remove-class obj "inspector-action")
  (clog:add-class obj "inspector-action-active")
  (let* ((hv:*get-input-fn* (get-input-fn pane-clog-obj))
         (result (hv:eval-thunk target)))
    (clog:remove-class obj "inspector-action-active")
    (clog:add-class obj "inspector-action")
    result))

(defun get-input-fn (clog-obj)
  #'(lambda (html-id)
      (let* ((element (clog:attach-as-child clog-obj html-id
                                            :clog-type 'clog:clog-form-element))
             (type (clog:attribute element "type")))
        (cond
          ((or (string= type "checkbox")
               (string= type "radio"))
           (let ((v
                   (if (string= "true" (clog:property element "checked"))
                       (clog:value element)
                       nil)))
             v))
          (t (clog:value element))))))

(defun set-view-var-fn  (clog-obj)
  #'(lambda (id value)
      nil))

;;
;; CSS
;;

(defparameter *css*
"
button {
  font-size: 100%;
  font-family: inherit;
  border: 0;
  padding: 0;
}
.inspector {
  /* Flex container properties, items are inspector-pane */
  display: flex;
  flex-flow: row nowrap;
  justify-content: start;
  /* End of flex container properties */
  height: 98vh;
  overflow: auto hidden;
}
.inspector-error {
  color: red;
}
.inspector-index {
  color: #888;
  padding: 3px 0px;
}
.inspector-pane {
  /* Flex item properties, container is inspector */
  /* set in style attribute: flex-basis: 600px; */
  /* set in style attribute: min-width: min(600px, 95%); */
  /* End of flex item properties */
  height: 100%;
  margin: 0px 10px;
  border: 1px solid #ccc;
  box-sizing:border-box;
  /* Flex container properties, items are title-bar, tabs, and body */
  display: flex;
  flex-flow: column nowrap;
  /* End of flex container properties */
}
.inspector-pane[hidden]{
  display:none;
}
.inspector-pane:focus {
  outline-style: solid;
  outline-color: #888;
  outline-width: thick;
}
.inspector-pane .inspector-minimize-button {
  display: none;
}
.inspector-pane .inspector-maximize-button {
  display: inline;
}
.inspector-pane-maximized {
  /* Flex item properties, container is inspector */
  /* set in style attribute: flex-basis: 100%; */
  /* set in style attribute: min-width: min(600px, 95%); */
  /* End of flex item properties */
  height: 100%;
  margin: 0px 10px;
  border: 1px solid #ccc;
  box-sizing:border-box;
  /* Flex container properties, items are title-bar, tabs, and body */
  display: flex;
  flex-flow: column nowrap;
  /* End of flex container properties */
}
.inspector-pane-maximized .inspector-minimize-button {
  display: inline;
}
.inspector-pane-maximized .inspector-maximize-button {
  display: none;
}
.inspector-title-bar {
  /* Flex item properties, container is inspector-pane */
  flex: 0 0 auto;
  /* End of flex item properties */
  /* Flex container properties,
     items are title-bar-class, title-bar-object, and title-bar-action-buttons */
  display: flex;
  flex-flow: row nowrap;
  justify-content: start;
  align-items: center;
  /* End of flex container properties */
  background-color: #ccc;
  padding: 5px 6px;
  white-space: nowrap;
}
.inspector-title-bar .inspector-title {
  overflow: hidden;
  flex-shrink: 5;
}
.inspector-title-bar .inspector-title:focus {
  background-color: #999;
}
.inspector-title-bar .inspector-title-bar-separator {
  margin-left: auto;
}
.inspector-title-bar .inspector-button {
  background-color: #ccc;
  flex-shrink: 0;
  float: right;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 0px 5px;
}
.inspector-title-bar .inspector-button:focus {
  background-color: #999;
}
.inspector-title-bar .inspector-title-bar-action-buttons {
  flex-shrink: 0;
  float: right;
}
.inspector-title-bar .inspector-action {
  color: black;
  background-color: #fff;
  padding: 2px 2px;
  border-style: solid;
  border-width: 1px;
}
.inspector-title-bar .inspector-action:focus {
  background-color: #ddd;
}
.inspector-title-bar .inspector-action-active {
  color: white;
  background-color: black;
}
.inspector-title-bar-class {
  background-color: #ccc;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 0px 5px;
}
.inspector-title-bar-class:hover {
  background-color: #aaa;
}
.inspector-title-bar-class:focus {
  background-color: #aaa;
}
.inspector-title-bar-object {
  background-color: #ccc;
  overflow: hidden;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 0px 5px;
}
.inspector-title-bar-object:hover {
  background-color: #aaa;
}
.inspector-title-bar-object:focus {
  background-color: #aaa;
}
.inspector-title-bar-action-buttons {
  padding: 0px 5px;
}
.inspector-tabs {
  /* Flex item properties, container is inspector-pane */
  flex: 0 0 auto;
  /* End of flex item properties */
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}
.inspector-tabs button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 5px 6px;
}
.inspector-tabs button:hover {
  background-color: #ddd;
}
.inspector-tabs button:focus {
  background-color: #ddd;
}
.inspector-tabs button.active {
  background-color: #ccc;
}
.inspector-body {
  /* Flex item properties, container is inspector-pane */
  flex: 1 1 auto;
  /* End of flex item properties */
  overflow: auto;
  padding: 5px;
}
.inspector-view {
  height: 100%;
}
.inspector-inspect {
  background-color: inherit;
  border: none;
  outline: none;
  cursor: pointer;
}
.inspector-inspect:hover {
  background-color: #ddd;
}
.inspector-inspect.inspector-highlight {
  background-color: #ddd;
}
.inspector-inspect.inspector-highlight:hover {
  background-color: #bbb;
}
.inspector-table {
  white-space: nowrap;
}
.inspector-table th {
  text-align: left;
  color: #aaa;
  padding: 0 5px;
}
.inspector-table td {
  padding: 0 5px;
}
.inspector-tree li {
  display: block;
}
.inspector-text {
  text-wrap: wrap;
  word-wrap: normal;
  white-space: pre;
}
.inspector-playground button {
  padding: 5px;
  margin: 10px 0px;
}
.inspector-markdown-cell {
  border-style: solid;
  border-width: thin;
  margin: 5px 0;
  padding: 2px;
}
.inspector-markdown-cell:focus {
  outline: none;
  border-style: double;
  border-width: thick;
}
.inspector-code-cell {
  margin: 5px 0;
}
.inspector-code-cell:focus {
  outline-style: solid;
}
pre {
  margin: 0 0;
}
lisp-toplevel[highlight] {
  display: block;
  background-color: #ddd;
}
lisp-number, lisp-character, lisp-string, lisp-pathname, lisp-array {
  color:#800;
}
lisp-keyword, lisp-uninterned-symbol {
  color:#397300;
}
lisp-symbol[package=\"COMMON-LISP\"], lisp-symbol[package=\"CL\"] {
  font-weight:700;
}
lisp-symbol a {
  text-decoration: none;
  color: inherit;
}
lisp-symbol a:hover {
  background-color: #ddd;
}
lisp-symbol[id]:hover {
  background-color: #ddd;
}
lisp-toplevel[highlight] a:hover {
  background-color: #bbb;
}
lisp-toplevel[highlight] lisp-symbol[id]:hover {
  background-color: #bbb;
}
")

;;
;; Open an inpector in a new browser tab or window.
;; The details of how this is done depends on how clog-inspect is called,
;; in particular if it's called from a CLOG application or not. One
;; strategy is assigning the object to *object* and then
;; opening the inspector URL in a browser. The browser connecting
;; to the Lisp process causes on-new-inspector to be called, which
;; reads *object*. This should be OK for the typical interactive
;; use case, but if you call clog-inspect in a loop, you will probably
;; get a mess.
;;

(defvar *object* nil)
(defvar *pane-width* nil)

(defun on-new-inspector (body &key (object *object*) (pane-width *pane-width*))
  (let ((sb (clog:create-style-block (clog:connection-body body))))
    (setf (clog:text sb) *css*))
  (when (typep body 'clog:clog-body)
    (let ((html-document (clog:html-document body)))
      (setf (clog:title html-document) "Inspector")))
  (let ((inspector (create-inspector body :pane-width pane-width)))
    (create-pane inspector object)))

(defun clog-inspect (&key (object hvs:*image*)
                          (clog-obj clog-user:*body*)
                          (use-popup t)
                          (base-url "/inspector")
                          (pane-width "600px"))
 "If CLOG is not running, CLOG is started. If CLOG-OBJ is nil, BASE-URL is
installed as the path for inspector windows. If CLOG-OBJ is set and USE-POPUP
is T, then a popup window will be generated parented to CLOG-OBJ. If USE-POPUP
is NIL, CLOG-OBJ is assumed to be the parent of the inspector."
 (unless (clog:is-running-p)
   (clog:initialize nil)
   (clog:enable-clog-popup))
 (unless clog-obj
   (clog:set-on-new-window #'on-new-inspector :path base-url))
 (setf clog-obj (if clog-obj
                    (if use-popup
                        (clog:open-clog-popup clog-obj)
                        clog-obj)))
 (if clog-obj
     (when (clog:validp clog-obj)
       (on-new-inspector clog-obj :object object :pane-width pane-width))
     (progn
       (setf *object* object)
       (setf *pane-width* pane-width)
       (clog:open-browser
         :url (format nil "http://127.0.0.1:~A/inspector" clog:*clog-port*)))))

;;
;; Shorthands for convenience
;;

(defun clog-inspect-package (&optional string-or-symbol)
 (if string-or-symbol
     (clog-inspect :object (find-package string-or-symbol))
     (clog-inspect :object *package*)))

(defun clog-inspect-system (string-or-symbol)
 (clog-inspect :object (asdf:find-system string-or-symbol)))

(defun clog-inspect-class (string-or-symbol)
 (clog-inspect :object (find-class string-or-symbol)))
