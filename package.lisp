;;;; Package definition
;;
;;;; Copyright (c) 2024-2025 Konrad Hinsen <konrad.hinsen@fastmail.net>

(defpackage :clog-moldable-inspector
  (:use :cl)
  (:local-nicknames (:hv :html-inspector-views) (:hvs :html-inspector-views/standard))
  (:import-from :arrow-macros
   :-> :-<> :->> :-<>> :<> :some-> :some->>)
  (:import-from :alexandria
   :if-let :when-let)
  (:export :clog-inspect
           :clog-inspect-package
           :clog-inspect-system
           :clog-inspect-class
           :clog-view
           :tutorial :*tutorial*))
