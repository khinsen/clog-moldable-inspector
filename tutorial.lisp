;;;; Built-in tutorial
;;
;;;; Copyright (c) 2024-2025 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package :clog-moldable-inspector)

;;
;; Tutorial infrastructure
;;

(defclass tutorial (hv:gui-class) ())

(defvar *tutorial* (make-instance 'tutorial :pane-title "Tutorial"))

(defun tutorial ()
 (clog-inspect :object *tutorial*))

;;
;; Views
;;

(hv:defview 👀overview (object tutorial)
  (hv:html-view :title "Overview" :priority 1
    (hv:html
      (:h4 (hv:esc "What is a moldable inspector?"))
      (:p (hv:esc "As a Common Lisp user, you have probably seen an inspector
before. But what makes an inspector ") (:i (hv:esc "moldable")) (hv:esc "?"))
      (:p (:a :href "https://moldabledevelopment.com/" "Moldable development")
          (hv:esc " is about development tools being customizable to fit the
 needs of a specific situation, or even a specific problem being investigated.
This requires customization to be cheap, to the point that it becomes feasible
to write single-use tools. You can think of a Unix shell as an early moldable
tool, since it can be customized by writing shell scripts."))
      (:p (hv:esc "This inspector is customizable by adding or overriding ")
          (:i (hv:esc "views")) (hv:esc " for specific classes or objects.
In an inspector pane, such as the one you are looking at, each view occupies
one tab. By clicking on a tab, you switch to a different view."))
      (:p (hv:esc "A view consists of HTML code with embedded references
(to other objects) and action buttons. For the details, see \"Defining views\""))
      (:h4 (hv:esc "Basic usage"))
      (:p (hv:esc "To open an inspector, call the function ")
          (:ul (:li (hv:object-ref #'clog-inspect :highlight t)))
          (hv:esc "and give it the object to inspect as keyword argument ")
          (:code (hv:esc ":object")) (hv:esc ". Without that argument, you get ")
          (hv:object-ref hvs:*image*
                         :display #'(lambda (_)
                                      (declare (ignore _))
                                      "an inspector")
                         :highlight t)
          (hv:esc " showing the systems, packages, and features present in your
image."))
      (:p (hv:esc "Click on the function name above to inspect the function!"))
      (:p (hv:esc "If you use the inspector often, it is worth defining a shortcut in your default workspace package. Here is mine:")
          (:pre (:code "
(defun inspect (object)
  (clog-moldable-inspector:clog-inspect :object object))")))
      (:p (hv:esc "There are a few convenience functions as well (again, click on the function names):"))
      (:ul (:li (hv:object-ref #'clog-inspect-package :highlight t))
           (:li (hv:object-ref #'clog-inspect-system :highlight t))
           (:li (hv:object-ref #'clog-inspect-class :highlight t))))))

(hv:defview 👀interface (object tutorial)
  (hv:html-view :title "Interface" :priority 2
    (hv:html
      (:h4 (hv:esc "Navigation"))
      (:p (hv:esc "Each pane of the inspector shows all the views for one object.
There are two kinds of objects: GUI objects, and standard objects."))
      (:p (hv:esc "GUI objects exist only as a support for inspector panes.
This tutorial is an example. The title bar for GUI objects shows just a title."))
      (:p (hv:esc "The title bar for standars objects shows the object's class
in italics, followed by a textual representation that is customizable. You
can click on the class name to inspect the class, and click on the textual
representation to open a second pane for the same object."))
      (:p (hv:esc "There are three buttons on the right-hand side of the
title bar:"))
      (:table :class "inspector-table"
              (:tr
               (:td (hv:str *icon-refresh*))
               (:td (hv:esc "redraws the pane.")))
              (:tr
               (:td )
               (:td (hv:esc "Use it when the object has
changed, or after modifying one of its views.")))
              (:tr
               (:td (hv:str *icon-maximize*))
               (:td (hv:esc "expands the pane to full width.")))
              (:tr
               (:td )
               (:td (hv:esc "The button then turns into")
                    (hv:str *icon-maximize*)
                    (hv:esc "for reducing the pane to its standard size.")))
              (:tr
               (:td (hv:str *icon-close*))
               (:td (hv:esc "closes the pane."))))
      (:p (hv:esc "Below the title bar, a tab bar shows one tab for each view.
Click on any tab to display its view."))
      (:p (hv:esc "When you click on an object to inspect it in a new pane,
by default the new pane replaces all the panes to the right. Use shift-click
to keep all panes, and add the new one to the right."))
      (:h4 (hv:esc "Introspection"))
      (:p (hv:esc "Alt-click on a view tab inspects the view itself. You
will see in particular the source code of the method that generated the view,
but also its HTML code."))
      (:p (hv:esc "Alt-click on the title bar (outside of the class name, the
textual representation, and the three buttons) to inspect the pane object.
This provides access to the title bar, the views, and the CLOG proxy object
for the DOM element representing the pane.")))))

(hv:defview 👀views (object tutorial)
  (hv:html-view :title "Views" :priority 3
    (hv:html
      (:h4 (hv:esc "Standard views"))
      (:p (hv:esc "All standard objects (i.e. all except GUI objects) have four
standard views."))
      (:p (:b (hv:esc "Slots")) (hv:esc " shows the slots of the object with
their values."))
      (:p (:b (hv:esc "Print")) (hv:esc " shows the object's print representation,
as defined by the generic function ")
          (hv:object-ref #'cl:print-object :highlight t)
          (hv:esc "."))
      (:p (:b (hv:esc "Operations")) (hv:esc " shows all generic functions in
the image that have a specialized for the object's class or one of its
superclasses, and which can be called with a single argument. The functions
are grouped by package name. You can click on the function name to inspect the
method for the object's class. You can also click on the triangle on the left
to run the method and inspect its return value."))
      (:p (:b (hv:esc "Playground")) (hv:esc " shows a text editor in which you
can enter Lisp code. When you click on the \"Eval\" button, the code is run and
its return value is inspected. The code is run with ")
          (hv:object-ref 'cl:*package* :highlight t)
          (hv:esc " set to a special package that imports all definitions from ")
          (hv:object-ref (find-package 'common-lisp) :highlight t)
          (hv:esc " and from the package to which the object's class belongs."))
      (:h4 (hv:esc "Custom views"))
      (:p (hv:esc "Non-standard views are defined for each class (and,
implicitly, its subclasses), via a ")
          (:code (hv:esc "defview"))
          (hv:esc " form. This is a thin wrapper around ")
          (:code (hv:esc "defgeneric"))
          (hv:esc "/")
          (:code (hv:esc "defmethod"))
          (hv:esc " whose main role is to add the generic function to a global
registry of view-generating functions. When an inspector pane is created,
all known view generators are called on the object to be inspected, and
those that return a ")
          (hv:object-ref (find-class 'hv:html-view) :highlight t)
          (hv:esc " object (rather than ")
          (hv:object-ref nil  :highlight t)
          (hv:esc ") are added to the pane, sorted by priority."))
      (:p (hv:esc "There are many views for classes that are part of the
Common Lisp standard, such as numbers, strings, functions, pathnames, etc.
You can add views for your own classes, but also for classes from other
packages that you use."))
      #+quicklisp (:p (hv:esc "There is also a GUI object representing ")
                      (hv:object-ref hvs:*quicklisp*
                                     :display #'(lambda (_)
                                                  (declare (ignore _))
                                                  "Quicklisp")
                                     :highlight t)
                      (hv:esc ", whose views provide a GUI layer on top of
the Quicklisp API.")))))

(hv:defview 👀actions (object tutorial)
  (hv:html-view :title "Actions" :priority 4
    (hv:html
      (:h4 (hv:esc "Action buttons"))
      (:p (hv:esc "You can put action buttons anywhere in a view.
An action button has a thunk attached to it that is executed when
the button is clicked. If the thunk returns non-nil, the inspector
pane is refreshed after the action. This is intended for actions that
change the object and thus invalidate its views."))
      (:p (hv:esc "Action buttons can also be added to a pane's title bar,
by specializing the generic function ")
          (hv:object-ref #'hv:title-bar-action-buttons :highlight t)
          (hv:esc ". An example is the \"copy\" button on pathnames,
which copies the pathname to the system clipboard. Check it out here: ")
          (hv:object-ref (user-homedir-pathname) :highlight t))
      (:p (hv:esc "By alt-clicking an action button, you can inspect the
thunk that represents the action. Try it on the \"copy\" button!"))
      (:h4 (hv:esc "Eval buttons"))
      (:p (hv:esc "Eval buttons differ from action buttons in one respect:
the return value of the action thunk is inspected in a new pane. The
arrow buttons in each object's \"Operations\" view are good examples.")))))

(hv:defview 👀writing-views (object tutorial)
  (hv:html-view :title "Writing views" :priority 5
    (hv:html
      (:h4 (hv:esc "View objects"))
      (:p (hv:esc "A view defined by ")
          (:code (hv:esc "defview"))
          (hv:esc " must return ")
          (:code (hv:esc "nil"))
          (hv:esc " or an instance of class ")
          (hv:object-ref (find-class 'hv:view) :highlight t)
          (hv:esc ", which has two subclasses:"))
      (:ul
       (:li (hv:object-ref (find-class 'clog-view) :highlight t)
            (:p (hv:esc " is for views defined by Lisp code using the CLOG API.
This is the most flexible form of view, but also the most laborious to write.
For an example, see ")
                (hv:object-ref #'👀playground :highlight t)
                (hv:esc ".")))
       (:li (hv:object-ref (find-class 'hv:html-view) :highlight t)
            (:p (hv:esc " is for views defined in terms of HTML code plus
an ALIST mapping HTML ids to Lisp objects. This ALIST is used to define
clickable references inside a view, in particular links to other objects
to be inspected, action buttons, and eval buttons."))))

      (:h4 (hv:esc "Writing HTML view definitions"))
      (:p (hv:esc "Since HTML + ALIST is the most common type of view,
there is an extensive support library for writing them, which is best explored
by looking at view definitions by alt-clicking. This support code uses the ")
          (hv:object-ref (find-package 'cl-who) :highlight t)
          (hv:esc " library for producing HTML code, so it's recommended
to read its ")
          (:a :href "https://edicl.github.io/cl-who/" (hv:esc "manual"))
          (hv:esc "."))

      (:h4 (hv:esc "Transclusions"))
      (:p (hv:esc "You can insert a view into another view, a process known as ")
          (:a :target "_blank"
              :href "https://en.wikipedia.org/wiki/Transclusion"
              (hv:esc "transclusion"))
          (hv:esc ". As a simple example, here is a transcluded view showing the
items of the list ")
          (:code (hv:esc "'(1 2)"))
          (hv:esc ":"))
      (hv:transclusion (hv:👀items '(1 2)))
      (:p (hv:esc "Transclusion is particularly useful for inserting the source
code of classes or functions in the system. For example, this is the source
code for the function ")
          (:code (hv:esc "transclusion"))
          (hv:esc ":"))
      (hv:transclusion (hvs:source-code-view #'hv:transclusion))

      (:h4 (hv:esc "Title bars"))
      (:p (hv:esc "The title bar of an inspector pane consists of two
single-line HTML views, constructed by methods of two generic functions:"))
      (:ul
       (:li (hv:object-ref #'hv:title-bar :highlight t)
            (:p (hv:esc " constructs the left-hand part, containing by
default the class and the object's textual representation.")))
       (:li (hv:object-ref #'hv:title-bar-action-buttons :highlight t)
            (:p (hv:esc " constructs the right-hand part, placed right
before the refresh/maximize/close buttons, which is empty by default.
It is meant to be used only for placing action buttons into the title bar."))))
      (:p (hv:esc "There are very few specializers for ")
          (hv:object-ref #'hv:title-bar :highlight t)
          (hv:esc " because most often it is only the textual
representation that needs to be customized. It is constructed by the
generic function ")
          (hv:object-ref #'hv:title-bar-representation :highlight t)
          (hv:esc ", which by default simply calls ")
          (hv:object-ref #'hv:html-representation :highlight t)
          (hv:esc ", the generic function that is also used by several
HTML views, e.g. to show an object inside a list or a vector. Its default
implementation calls ")
          (hv:object-ref #'hv:text-representation :highlight t)
          (hv:esc ", which return a plain-text representation of an object.
In most cases, specializing this function is sufficient. Check out its
\"Methods\" view to see examples!")))))

(hv:defview 👀forms (object tutorial)
  (hv:html-view :title "Forms" :priority 6
    (let ((a-text (hv:input-id))
          (a-search (hv:input-id))
          (a-number (hv:input-id))
          (a-range (hv:input-id))
          (a-date (hv:input-id))
          (a-datetime (hv:input-id))
          (an-email (hv:input-id))
          (a-password (hv:input-id))
          (checkbox1 (hv:input-id))
          (checkbox2 (hv:input-id))
          (radio1 (hv:input-id))
          (radio2 (hv:input-id))
          (a-color (hv:input-id))
          (a-select (hv:input-id)))
      (hv:html
        (hv:esc "You can use input elements in views, and retrieve their values
in the code run by action and eval buttons. Here are some examples. Alt-click
the view name to see the source code, and in particular how to retrieve the
input values!")
        (:br)
        (:br)
        (:div
         (:label :for a-text (hv:esc "Text: "))
         (:input :type "text" :id a-text)
         (hv:eval-button "Get value" (hv:thunk (hv:get-input a-text))))
        (:br)
        (:div
         (:label :for a-search (hv:esc "Search: "))
         (:input :type "search" :id a-search)
         (hv:eval-button "Get value" (hv:thunk (hv:get-input a-search))))
        (:br)
        (:div
         (:label :for a-number (hv:esc "Number: "))
         (:input :type "number" :id a-number)
         (hv:eval-button "Get value" (hv:thunk (hv:get-input a-number))))
        (:br)
        (:div
         (:label :for a-range (hv:esc "Range: "))
         (:input :type "range" :id a-range
                 :min 1 :max 10)
         (hv:eval-button "Get value" (hv:thunk (hv:get-input a-range))))
        (:br)
        (:div
         (:label :for an-email (hv:esc "E-Mail: "))
         (:input :type "email" :id an-email)
         (hv:eval-button "Get value" (hv:thunk (hv:get-input an-email))))
        (:br)
        (:div
         (:label :for a-password (hv:esc "Password: "))
         (:input :type "password" :id a-password)
         (hv:eval-button "Get value" (hv:thunk (hv:get-input a-password))))
        (:br)
        (:div
         (:label :for a-date (hv:esc "Date: "))
         (:input :type "date" :id a-date)
         (hv:eval-button "Get value" (hv:thunk (hv:get-input a-date))))
        (:br)
        (:div
         (:label :for a-datetime (hv:esc "Date/time: "))
         (:input :type "datetime-local" :id a-datetime)
         (hv:eval-button "Get value" (hv:thunk (hv:get-input a-datetime))))
        (:br)
        (:div
         (:label :for a-color (hv:esc "Color: "))
         (:input :type "color" :id a-color)
         (hv:eval-button "Get value" (hv:thunk (hv:get-input a-color))))
        (:br)
        (:div
         (:fieldset
          (:legend (hv:esc "Select your choices:"))
          (:div
           (:input :type "checkbox" :id checkbox1 :value "one")
           (:label :for checkbox1 (hv:esc "One")))
          (:div
           (:input :type "checkbox" :id checkbox2 :value "two")
           (:label :for checkbox2 (hv:esc "Two"))))
         (:div
          (hv:eval-button "Get values" (hv:thunk (list (hv:get-input checkbox1)
                                                       (hv:get-input checkbox2))))))
        (:br)
        (:div
         (:fieldset
          (:legend (hv:esc "Make your choice:"))
          (:div
           (:input :type "radio" :id radio1 :name "choice" :value "one")
           (:label :for radio1 (hv:esc "One")))
          (:div
           (:input :type "radio" :id radio2 :name "choice" :value "two")
           (:label :for radio2 (hv:esc "Two"))))
         (:div
          (hv:eval-button "Get values" (hv:thunk (list (hv:get-input radio1)
                                                       (hv:get-input radio2))))))
        (:br)
        (:div
         (:label :for a-select (hv:esc "Make your choice: "))
         (:select :id a-select
                  (:option :value "" (hv:esc "Choose an option"))
                  (:option :value "one" (hv:esc "one"))
                  (:option :value "two" (hv:esc "two")))
         (:div
          (hv:eval-button "Get value" (hv:thunk (hv:get-input a-select)))))))))
